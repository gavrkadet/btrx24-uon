<?php

declare(strict_types=1);

use common\models\Leads;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var backend\models\LeadsSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="leads-index">

    <div class="row">
        <div class="col-6">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <?php Pjax::begin(['id' => 'pjaxContent']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn'
            ],
            [
                'class' => 'yii\grid\CheckboxColumn'
            ],
            'lead_id',
            'title',
            'name',
            'last_name',
            'second_name',
            [
                'attribute' => 'status_id',
                'value' => function (Leads $lead) {
                    return $lead->status->name_status;
                }
            ],
            'phone',
            'email',
            'comments',
        ],
    ]);
    ?>

    <?php Pjax::end(); ?>

    <div class="row">
        <p>
            <button class="btn btn-success" id="buttonToMove" type="submit">Send leads to U-ON</button>
        </p>
    </div>

    <script>
        //When clicking on a row, we change the state of the checkbox of this row
        $('.table > tbody > tr').on('click', function (data) {
            let idCheckbox = data.currentTarget.attributes[0].value
            let needCheckbox = $(`input[value = "${idCheckbox}"]`)
            if (needCheckbox.is(':checked')) {
                needCheckbox.prop('checked', false)
            } else {
                needCheckbox.prop('checked', true)
            }
        })
        //When you click on the checkbox of any line, change its state
        $('.table > tbody > tr > td > input').on('click', function (data) {
            let idCheckbox = data.currentTarget.attributes[2].value
            let needCheckbox = $(`input[value = "${idCheckbox}"]`)
            if (needCheckbox.is(':checked')) {
                needCheckbox.prop('checked', false)
            } else {
                needCheckbox.prop('checked', true)
            }
        })
        //ajax request when clicking the 'Send leads to U-ON' button
        $('#buttonToMove').on('click', function () {
            let arrayId = $('.grid-view').yiiGridView('getSelectedRows')
            if (arrayId.length !== 0) {
                $.ajax({
                        url: 'http://btrx24-uon/backend/web/index.php?r=leads%2Fmove',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            arrayId: arrayId,
                        },
                        success: function () {
                            $.pjax.reload({container: '#pjaxContent'});
                            confirm('Выбранные лиды успешно перенесены')
                        },
                        error: function (data) {
                            console.log(data.statusText)
                        }
                    }
                )
            } else
                alert('Вы забыли выбрать лиды для переноса')
        })
    </script>

</div>
