<?php

declare(strict_types=1);

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Request;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var string $stringStatuses */
/** @var string $stringCountStatuses */
/** @var Request $request */

$this->title = 'Diagram';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="leads-index">

    <div class="row">
        <div class="col-6">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <?php Pjax::begin(['id' => 'pjaxContent']); ?>
    <div>
        <canvas class="diagramLeads" id="bar" width="700" height="250"></canvas>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        const ctx = $('.diagramLeads');
        let chart = new Chart(ctx, {
            type: '<?= $request->get('type') === 'pie' ? 'pie' : 'bar' ?>',
            data: {
                labels: <?= $stringStatuses?>,
                datasets: [{
                    label: 'Количество лидов',
                    data: <?= $stringCountStatuses?>,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>

    <a href="<?= Url::current(['type' => $request->get('type') === 'pie' ? 'bar' : 'pie']) ?>">
        <p>
            <button class="btn btn-success" id="changeDiagramType" type="submit">Change type of diagram</button>
        </p>
    </a>
</div>
