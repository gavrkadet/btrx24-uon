<?php

use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var common\models\Leads $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="leads-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-5">
            <?= $form->field($model, 'header')->textInput(['maxlength' => true, 'placeholder' => 'Заголовок'])->label(false) ?>
        </div>

        <div class="col-2">
            <?= $form->field($model, 'phone_number')->widget(PhoneInput::class, [
                'jsOptions' => [
                    'preferredCountries' => ['ru'],
                ]
            ])->label(false);
            ?>
        </div>

        <div class="col-5">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Почта'])->label(false) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Фамилия'])->label(false) ?>
        </div>

        <div class="col-4">
            <?= $form->field($model, 'second_name')->textInput(['maxlength' => true, 'placeholder' => 'Имя'])->label(false) ?>
        </div>

        <div class="col-4">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'placeholder' => 'Отчество'])->label(false) ?>
        </div>
    </div>

    <div class="row">
        <?= $form->field($model, 'comment')->textarea(['maxlength' => true, 'placeholder' => 'Комментарий', 'rows' => 2])->label(false) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Готово', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
