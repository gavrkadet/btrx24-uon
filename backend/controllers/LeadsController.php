<?php

namespace backend\controllers;


use common\models\Leads;
use backend\models\LeadsSearch;
use common\models\Statuses;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use GuzzleHttp;

/**
 * LeadsController implements the CRUD actions for Leads model.
 */
class LeadsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * This action is triggered, when you reload page http://btrx24-uon/backend/web/index.php?r=leads%2Findex
     * Firstly, get array leads from bitrix24 using CRest library
     * Secondly, get array statuses from bitrix24 also using CRest
     * Thirdly, write all statuses in table 'statuses'
     * Fourthly, write all leads in table 'leads'
     * If lead exist in table 'leads', update his data, specifying oldAttribues and Attributes,
     * else write this firstly without oldAttributes (just Attributes).
     * @return string
     */
    public function actionIndex(): string
    {
        $arrayLeads = Leads::getAllLeads();         //Get array leads
        $arrayStatuses = Leads::getAllStatuses();   //Get array statuses
        //Write all statuses from bitrix in db
        foreach ($arrayStatuses['result'] as $status) {
            $model = new Statuses();
            $model->setNewAttributes($status);
            $model->save();
        }
        //Write all leads from bitrix in db
        foreach ($arrayLeads['result'] as $lead) {
            $model = new Leads();
            $model->setNewAttributes($lead);
            $searchLead = Leads::find()->where(['lead_id' => $model->attributes['lead_id']])->one();
            if ($searchLead) {
                //If lead exist in db, update his
                $model->oldAttributes = $searchLead->attributes;
            }//else write lead without oldAttributes
            $model->save();
        }
        $searchModel = new LeadsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @throws GuzzleHttp\Exception\GuzzleException
     * This action is triggered, when you click on the 'Send leads to U-ON' button
     * Firstly, gets apiKey from params.php
     * Secondly, find all leads from table 'leads', where checkbox was checked
     * Thirdly, for each found lead form HTTP request and specify, what data will be sent,
     * example: 'u_name' => $lead->attributes['name'] ...
     * If received response status code = 200, change fields for each lead, what has been sent
     */
    public function actionMove()
    {
        $config = require __DIR__ . '/../config/params.php';
        $apiKey = $config['apiKey'];                                //Get apiKey from config/params.php
        if ($arrayId = $this->request->post('arrayId')) {
            $searchLeads = Leads::find()                            //Find all leads, where checkbox was checked
                ->where(['id' => $arrayId])
                ->orderBy('id')
                ->all();
            $client = new GuzzleHttp\Client();
            foreach ($searchLeads as $lead) {
                $res = $client->request('POST',             //Send selected leads to u-on
                    'https://api.u-on.ru/' . $apiKey . '/lead/create', [
                        'json' => [
                            'u_name'    => $lead->attributes['name'],
                            'u_surname' => $lead->attributes['last_name'],
                            'u_sname'   => $lead->attributes['second_name'],
                            'u_email'   => $lead->attributes['email'],
                            'u_phone'   => $lead->attributes['phone'],
                            'note'      => $lead->attributes['comments'],
                        ]
                    ]);
                if ($res->getStatusCode() === 200) {
                    //If response is Ok, change field `exist_uon`
                    Leads::updateAll(['exist_uon' => true], ['id' => $arrayId]);
                }
            }
            echo 1;
        } else {
            echo 'Error get arrayId from request';
        }
    }

    /**
     * This action is triggered, when you click on the 'Show Diagram'
     * Firstly, get all lead statuses from table 'statuses'
     * Secondly, form array of status names
     * Thirdly, form string status names for using in ChartJS options
     * Fourthly, counting the number of leads for each status and form string counts for every status
     * Variable $request need, that you can change type of diagram (bar or pie).
     * In diagram.php use stringStatuses and stringCountStatuses in options ChartJS.
     * $request contain info about type of diagram (if type == null, diagram type - bar)
     */
    public function actionDiagram(): string
    {
        $arrayStatuses = [];
        //Get all lead statuses
        $allStatuses = Statuses::find()
            ->select(['id', 'name_status'])
            ->all();
        //Form array of status names
        foreach ($allStatuses as $status) {
            $arrayStatuses[] = $status['attributes']['name_status'];
        }
        //Form string status names
        $stringStatuses = "['" . implode("', '", $arrayStatuses) . "']";
        $arrayCount = [];
        //Counting the number of leads for each status
        foreach ($allStatuses as $status) {
            $count = Leads::find()
                ->where(['status_id' => $status->attributes['id']])
                ->count();
            $arrayCount[$status->attributes['name_status']] = $count;
        }
        //Form string counts for every status
        $stringCountStatuses = "['" . implode("', '", $arrayCount) . "']";
        $request = $this->request; //$request contain info about type of diagram (if type == null, type = bar)
        unset($arrayCount, $allStatuses, $arrayStatuses);
        return $this->render(
            'diagram',
            compact('stringStatuses', 'stringCountStatuses', 'request')
        );
    }

    /**
     * Displays a single Leads model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Leads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Leads();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Leads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Leads model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Leads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Leads the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Leads::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
