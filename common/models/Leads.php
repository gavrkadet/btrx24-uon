<?php

declare(strict_types=1);

namespace common\models;

require_once('../../vendors/crest/src/crest.php');

use CRest;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\Response;


/**
 * This is the model class for table "leads".
 *
 * @property int $id
 * @property int $lead_id
 * @property string|null $title
 * @property string|null $name
 * @property string|null $status_id
 * @property string|null $second_name
 * @property string|null $last_name
 * @property int|null $phone
 * @property string|null $email
 * @property string|null $comments
 * @property Statuses $status
 */
class Leads extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'leads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone'], 'integer'],
            [['id', 'lead_id'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['name', 'second_name', 'last_name', 'status_id'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 20],
            [['comments'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lead_id' => 'ID Лида',
            'title' => 'Заголовок',
            'name' => 'Имя',
            'second_name' => 'Отчество',
            'last_name' => 'Фамилия',
            'status_id' => 'Статус',
            'phone' => 'Номер Телефона',
            'email' => 'Почта',
            'comments' => 'Комментарий',
        ];
    }

    /**
     * This API return info about all leads. In select options specify need fields.
     */
    public static function getAllLeads(): array
    {
        return CRest::call(
            'crm.lead.list',
            [
                'select' => [
                    'ID',
                    'TITLE',
                    'NAME',
                    'STATUS_ID',
                    'SECOND_NAME',
                    'LAST_NAME',
                    'PHONE',
                    'EMAIL',
                    'COMMENTS'
                ]
            ],
        );
    }

    /**
     * This API return info about all lead statuses.
     */
    public static function getAllStatuses()
    {
        return CRest::call(
            "crm.status.list",
            [
                'order' => ["SORT" => "ASC"],
                'filter' => ["ENTITY_ID" => "STATUS"],
            ]);
    }

    /**
     * This method sets attribues of an object of the Leads class. Also, if exist phone and email,
     * get them from the array.
     */
    public function setNewAttributes($arrayAttributes)
    {
        if (array_key_exists('PHONE', $arrayAttributes)
            && is_array($arrayAttributes['PHONE'])) {
            $this->phone = $arrayAttributes['PHONE'][0]['VALUE'];
        }
        if (array_key_exists('EMAIL', $arrayAttributes)
            && is_array($arrayAttributes['EMAIL'])) {
            $this->email = $arrayAttributes['EMAIL'][0]['VALUE'];
        }
        $this->lead_id      = $arrayAttributes['ID'];
        $this->title        = $arrayAttributes['TITLE'];
        $this->name         = $arrayAttributes['NAME'];
        $this->status_id    = $arrayAttributes['STATUS_ID'];
        $this->second_name  = $arrayAttributes['SECOND_NAME'];
        $this->last_name    = $arrayAttributes['LAST_NAME'];
        $this->comments     = trim($arrayAttributes['COMMENTS'], '<br>');
    }

    /**
     * Creates a new Leads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     */
    public function getStatus(): ActiveQuery
    {
        return $this->hasOne(Statuses::class, ['id' => 'status_id']);
    }

}
