<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "statuses".
 *
 * @property string $id
 * @property string $name_status
 *
 * @property Leads[] $leads
 */
class Statuses extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'statuses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name_status'], 'required'],
            [['name_status'], 'string', 'max' => 50],
            [['id'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name_status' => 'Name Status',
        ];
    }

    /**
     * This method sets attribues of an object of the Statuses class. Also, if exist phone and email,
     * get them from the array.
     */
    public function setNewAttributes($arrayAttributes)
    {
        $this->id = $arrayAttributes['STATUS_ID'];
        $this->name_status = $arrayAttributes['NAME'];
    }
}
