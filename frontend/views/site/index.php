<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="p-5 mb-4 bg-transparent rounded-3">
        <div class="container-fluid py-5 text-center">
            <h1 class="display-4">Тестовое задание</h1>
            <p class="fs-5 fw-light">Для того, чтобы начать работу, Вам необходимо зарегистрироваться.
                Нажмите на вкладку "Sign up" и введите логин и пароль. Затем, перейдите на вкладку "Go
                To Admin". Введите ваши логин и пароль и Вы сможете работать с лидами. </p>
        </div>
    </div>

</div>
