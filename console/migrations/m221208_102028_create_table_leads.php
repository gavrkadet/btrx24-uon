<?php

use yii\db\Migration;

/**
 * Class m221208_102028_create_table_leads
 */
class m221208_102028_create_table_leads extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('leads', [
            'id' => $this->primaryKey(),
            'lead_id' => $this->integer(50)->notNull(),
            'title' => $this->string(100),
            'name' => $this->string(50),
            'status_id' => $this->string(50)->notNull(),
            'second_name' => $this->string(50),
            'last_name' => $this->string(50),
            'phone' => $this->string(20),
            'email' => $this->string(20),
            'comments' => $this->string(1000),
            'exist_uon' => $this->boolean()->defaultValue(false),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'status_id_fk',
            'leads',
            'status_id',
            'statuses',
            'id',
            'CASCADE',
            'CASCADE',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('leads');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221208_102028_create_table_leads cannot be reverted.\n";

        return false;
    }
    */
}
