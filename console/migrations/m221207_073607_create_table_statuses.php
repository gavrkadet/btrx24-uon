<?php

use yii\db\Migration;

/**
 * Class m221207_073607_create_table_statuses
 */
class m221207_073607_create_table_statuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('statuses', [
            'id' => $this->string(),
            'name_status' => $this->string(50)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('id', 'statuses', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropPrimaryKey('id', 'statuses');
        $this->dropTable('statuses');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221207_073607_create_table_statuses cannot be reverted.\n";

        return false;
    }
    */
}
